function hideResults() {
    document.getElementById("results").style.display = "none";
} 

function play() {
    var startingBet = document.getElementById("startingBet").value;
    var bet = startingBet;
	var dice1 = Math.floor(Math.random() * 6) + 1;
	var dice2 = Math.floor(Math.random() * 6) + 1;
	var diceRoll = dice1 + dice2;
    var betsRange = [];

    while (bet > 0) {
		var dice1 = Math.floor(Math.random() * 6) + 1;
		var dice2 = Math.floor(Math.random() * 6) + 1;
		var diceRoll = dice1 + dice2;
		if(diceRoll != 7) {
            bet -= 1
        } else { 
            bet += 4
        }
        betsRange.push(bet)
		
    }

    var rollCounter = betsRange.length;
    var highestAmount = Math.max.apply(Math, betsRange);
    var highestPosition = betsRange.indexOf(highestAmount);
    var rollsFromHighest = rollCounter - highestPosition;

    function showResults() {
    document.getElementById("results").style.display = "inline";
    document.getElementById("playButton").innerHTML = "Play Again";
    document.getElementById("resultsBet").innerHTML = "$" + startingBet +".00";
    document.getElementById("resultsRollCounter").innerHTML = rollCounter;
    document.getElementById("resultsHighestHeld").innerHTML = "$" +     highestAmount + ".00";
    document.getElementById("resultsRollsFromHighest").innerHTML = rollsFromHighest;
    };

    showResults();
} 